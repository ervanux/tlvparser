//
//  TLV.swift
//  Hello
//
//  Created by Erkan Ugurlu [Dijital Inovasyon Atolyesi] on 13/03/2017.
//
//

import Foundation

class TLV : CustomStringConvertible {
    
    var value:Data?
    var description: String {
        guard let value = self.value else {
            return "no data"
        }
        return TLV.parse(data:value)
    }
    
    init(_ value:Data) {
        self.value = value
    }
    
    init(_ base64Encoded:String) {
        self.value = base64Encoded.padding(toLength: base64Encoded.characters.count+4-base64Encoded.characters.count%4, withPad: "=", startingAt: 0).decodeUrlsafeBase64()
    }
    
    func get(tag:TagsEnum) -> TLV? {
        guard let value = self.value  else {
            return nil
        }
        let byteReader = Reader(data: value)
        while (!byteReader.endOfStream()) {
            let tagInt = byteReader.readUInt16()
            let length = byteReader.readInt16()
            let value = byteReader.read(Int(length))
            
            if (tag == TagsEnum(rawValue: tagInt)) {
                return TLV(value)
            }
        }
        
        return nil
    }
    
    static func parse(base64Encoded: String) -> String {
        if let bytes = base64Encoded.padding(toLength: base64Encoded.characters.count+4-base64Encoded.characters.count%4, withPad: "=", startingAt: 0).decodeUrlsafeBase64() {
            return parse(data: bytes)
        } else {
            return "Encoding problem"
        }
    }
    
    static func parse(space : String = "  ", data: Data)  -> String {
        var result = ""
        let byteReader = Reader(data: data)
        while (!byteReader.endOfStream()) {
            let tagInt = byteReader.readUInt16()
            guard let tag = TagsEnum(rawValue: tagInt)
                else {
                print("Unknown tag value \(String(tagInt, radix:16))")
                    break
            }
            let length_i16 = byteReader.readInt16()
            let length = Int(length_i16)
            let value = byteReader.read(length)
            result += "\(space)\(tag)(length:\(length), value:\(self.getDataToValue(data:value)))" + "\n"

            if value.count > 4 {
                result += self.parse(space: space + "  ", data: value)
            }
        }
        return result
    }
    
    static func getDataToValue(data:Data) -> String {
        if data.count == MemoryLayout<Int8>.size {
            return "\(data.to(type: Int8.self))"
        } else if data.count == MemoryLayout<Int16>.size {
            return "\(data.to(type: Int16.self))"
        } else if data.count == MemoryLayout<Int32>.size {
            return "\(data.to(type: Int32.self))"
            //        } else if data.count == MemoryLayout<Int64>.size {
            //            return "\(data.to(type: Int64.self))"
        } else if let str = String(data:data, encoding:String.Encoding.utf8),data.filter({$0 < 32 || $0 > 127 }).count == 0{
            return str
        } else {
            return data.hexEncodedString()
        }
    }
    
}

class Reader {
    var pointer:Int
    var data:Data
    
    init(data:Data){
        self.data = data
        pointer = 0
    }
    
    func read<T:Integer>() -> T {
        return self.read(MemoryLayout<T>.size).to(type: T.self)
    }
    
    func readInt32() -> Int32 {
        return self.read()
    }
    
    func readInt16() -> Int16 {
        return self.read()
    }

    func readUInt16() -> UInt16 {
        return self.read()
    }
    
    func readInt8() -> Int8 {
        return self.read()
    }
    
    func read(_ length: Int) -> Data {
        let result = self.data.subdata(in: self.pointer..<self.pointer+length)
        self.pointer += length
        return result
    }
    
    func endOfStream() -> Bool {
        if self.pointer >= self.data.count - 1 {
            return true
        } else {
            return false
        }
        
    }
}

extension String{
    
    func decodeUrlsafeBase64() -> Data? {
        var base64String = self
        base64String = base64String.replacingOccurrences(of: "_", with: "/", options: NSString.CompareOptions.literal, range: nil)
        base64String = base64String.replacingOccurrences(of: "-", with: "+", options: NSString.CompareOptions.literal, range: nil)
        return Data(base64Encoded: base64String, options: .ignoreUnknownCharacters)
    }

}

protocol DataConvertible {
    init?(data: Data)
    var data: Data { get }
}

extension DataConvertible {
    
    init?(data: Data) {
        guard data.count == MemoryLayout<Self>.size else { return nil }
        self = data.withUnsafeBytes { $0.pointee }
    }
    var data: Data {
        var value = self
        return Data(buffer: UnsafeBufferPointer(start: &value, count: 1))
    }
}

extension Int : DataConvertible { }
extension Int8 : DataConvertible { }
extension Int16 : DataConvertible { }
extension UInt16 : DataConvertible { }
extension Int32 : DataConvertible { }
extension Int64 : DataConvertible { }
extension Float : DataConvertible { }
extension Double : DataConvertible { }

extension Data {
    
    mutating func appendTagAndData(_ tag:TagsEnum, data:Data?){
        self.append(Int16(tag.rawValue).data)
        //        self.appendData(self.encodeInt(tag.rawValue))
        if let data = data {
            self.append(Int16(self.getLength(tag,data: data)).data)
            self.append(data)
        } else {
            self.append(Int16(0).data)
        }
    }
    
    func getLength(_ withTag:TagsEnum,data:Data) -> Int {
        switch withTag {
            //        case .TAG_UAFV1_REG_ASSERTION:
        //            return 2
        default:
            return data.count
        }
    }
    
    func to<T>(type: T.Type) -> T {
        return self.withUnsafeBytes { $0.pointee }
    }
    
    func hexEncodedString() -> String {
        return map { String(format: " %02hhx", $0) }.joined()
    }
}

enum TagsEnum : UInt16{
    
    //4.1 Command Tags
    case tag_UAFV1_GETINFO_CMD = 0x3401, //Tag for GetInfo command.
    tag_UAFV1_GETINFO_CMD_RESPONSE = 0x3601, //Tag for GetInfo command response.
    tag_UAFV1_REGISTER_CMD = 0x3402, //Tag for Register command.
    tag_UAFV1_REGISTER_CMD_RESPONSE = 0x3602, //Tag for Register command response.
    tag_UAFV1_SIGN_CMD = 0x3403, //Tag for Sign command.
    tag_UAFV1_SIGN_CMD_RESPONSE = 0x3603, //Tag for Sign command response.
    tag_UAFV1_DEREGISTER_CMD = 0x3404, //Tag for Deregister command.
    tag_UAFV1_DEREGISTER_CMD_RESPONSE = 0x3604, //Tag for Deregister command response.
    tag_UAFV1_OPEN_SETTINGS_CMD = 0x3406, //Tag for OpenSettings command.
    tag_UAFV1_OPEN_SETTINGS_CMD_RESPONSE = 0x3606, //Tag for OpenSettings command response.
    
    //4.2 Tags used only in Authenticator Commands
    tag_KEYHANDLE = 0x2801, //Represents key handle.
    tag_KEYHANDLE_ACCESS_TOKEN = 0x2805, //Represents a key handle Access Token.
    tag_USERNAME = 0x2806, //A Username as a UINT8[] encoding of a UTF-8 string.
    tag_USERVERIFY_TOKEN = 0x2803, // Represents a User Verification Token.
    tag_APPID = 0x2804, //A full AppID as a UINT8[] encoding of a UTF-8 string.
    tag_ATTESTATION_TYPE = 0x2807, //Represents an Attestation Type.
    tag_STATUS_CODE = 0x2808, //Represents a Status Code.
    tag_AUTHENTICATOR_METADATA = 0x2809, //Represents a more detailed set of authenticator information.
    tag_ASSERTION_SCHEME = 0x280A, //A UINT8[] containing the UTF8-encoded Assertion Scheme as defined in [UAFRegistry]. ("UAFV1TLV")
    tag_TC_DISPLAY_PNG_CHARACTERISTICS = 0x280B, //If an authenticator contains a PNG-capable transaction confirmation display that is not implemented by a higher-level layer, this tag is describing this display. See [FIDOMetadataStatement] for additional information on the format of this field.
    tag_TC_DISPLAY_CONTENT_TYPE = 0x280C, //A UINT8[] containing the UTF-8-encoded transaction display content type as defined in [FIDOMetadataStatement]. ("image/png")
    tag_AUTHENTICATOR_INDEX = 0x280D, //Authenticator Index
    tag_API_VERSION = 0x280E, //API Version
    tag_AUTHENTICATOR_ASSERTION = 0x280F, //The content of this TLV tag is an assertion generated by the authenticator. Since authenticators may generate assertions in different formats - the content format may vary from authenticator to authenticator.
    tag_TRANSACTION_CONTENT = 0x2810, //Represents transaction content sent to the authenticator.
    tag_AUTHENTICATOR_INFO = 0x3811, //Includes detailed information about authenticator's capabilities.
    tag_SUPPORTED_EXTENSION_ID = 0x2812, //Represents extension ID supported by authenticator.
    tag_TRANSACTIONCONFIRMATION_TOKEN = 0x2813, //Represents a token for transaction confirmation. It might be returned by the authenticator to the ASM and given back to the authenticator at a later stage. The meaning of it is similar to tag_USERVERIFY_TOKEN, except that it is used for the user's approval of a displayed transaction text.
    
    //4.3 Tags used in UAF Protocol
    tag_UAFV1_REG_ASSERTION = 0x3E01, //Authenticator response to Register command.
    tag_UAFV1_AUTH_ASSERTION = 0x3E02, //Authenticator response to Sign command.
    tag_UAFV1_KRD = 0x3E03, //Key Registration Data
    tag_UAFV1_SIGNED_DATA = 0x3E04, //Data signed by authenticator with the UAuth.priv key
    tag_ATTESTATION_CERT = 0x2E05, //Each entry contains a single X.509 DER-encoded [ITU-X690-2008] certificate. Multiple occurrences are allowed and form the attestation certificate chain. Multiple occurrences must be ordered. The attestation certificate itself must occur first. Each subsequent occurrence (if exists) must be the issuing certificate of the previous occurrence.
    tag_SIGNATURE = 0x2E06, //A cryptographic signature
    tag_ATTESTATION_BASIC_FULL = 0x3E07, //Full Basic Attestation as defined in [UAFProtocol]
    tag_ATTESTATION_BASIC_SURROGATE = 0x3E08, //Surrogate Basic Attestation as defined in [UAFProtocol]
    tag_ATTESTATION_ECDAA = 0x3E09, //Elliptic curve based direct anonymous attestation as defined in [UAFProtocol]. In this case the signature in tag_SIGNATURE is a ECDAA signature as specified in [FIDOEcdaaAlgorithm].
    tag_KEYID = 0x2E09, //Represents a KeyID.
    tag_FINAL_CHALLENGE_HASH = 0x2E0A, //Represents a Final Challenge Hash.
    
    tag_AAID = 0x2E0B, //Represents an authenticator Attestation ID.
    
    tag_PUB_KEY = 0x2E0C, //Represents a Public Key.
    tag_COUNTERS = 0x2E0D, //Represents a use counters for the authenticator.
    tag_ASSERTION_INFO = 0x2E0E, //Represents assertion information necessary for message processing.
    tag_AUTHENTICATOR_NONCE = 0x2E0F, //Represents a nonce value generated by the authenticator.
    tag_TRANSACTION_CONTENT_HASH = 0x2E10, //Represents a hash of transaction content.
    tag_EXTENSION = 0x3E11,//,  = 0x3E12, //This is a composite tag indicating that the content is an extension.
    tag_EXTENSION_2 = 0x3E12,//,  = 0x3E12, //This is a composite tag indicating that the content is an extension.
    
    tag_EXTENSION_ID = 0x2E13, //Represents extension ID. Content of this tag is a UINT8[] encoding of a UTF-8 string.
    tag_EXTENSION_DATA = 0x2E14, //Represents extension data. Content of this tag is a UINT8[] byte array.
    
    tag_XTRONG_OTP_SHAREDKEY = 0x4E01,
    tag_XTRONG_OTP_CLIENTTIMEMILLIS = 0x4E02,
    tag_XTRONG_OTP = 0x4E03,
    tag_XTRONG_ONLINEPIN = 0x4E04,
    tag_XTRONG_OTP_INTERVAL = 0x4E05,
    tag_XTRONG_OTP_LENGTH = 0x4E06,
    tag_XTRONG_OTP_PUBLICKEY = 0x4E07,
    tag_XTRONG_OFFLINEPIN = 0x4E08
    
    
//    Unknown1=0x3030,
////    Unknown tag value 100
//    Unknown2=0x46d2,
//    Unknown3=0x6a04,
//    Unknown4=0xb73e,
//    Unknown5=0x4530,
//    Unknown6=0x3e00
//
    
}
