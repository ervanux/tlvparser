import Vapor
import HTTP
import Foundation

let drop = Droplet()

drop.get { req in
    var a = ""
    if let encoded64 = req.data["p"]?.string {
        a = TLV.parse(base64Encoded: encoded64)
        //        return "\(a)"
    }
    
    return try drop.view.make("tlv", [
        "message": drop.localization[req.lang, a]
        ])
}

//drop.get("tlv") { request in
//    var encoded64 = "ATZLAAgoAgAAAA4oAQABETg8AAsuCQA5ODc0IzAxMDEJKA0AAAABAgAEAAIAAAAGAAwoAgAAAAooCABVQUZWMVRMVgcoAgAHPg0oAQAaAA=="
//    //    if let encoded64 = request.data["p"]?.string {
//    let a = TLV.parse(base64Encoded: encoded64)
//    return "\(a)"
//    //    }
//
//    //    return ""
//}

enum XtrongEnv : String{
    case local
    case azure
    case global
    
}

func getUrl(environment:XtrongEnv?) -> String {
    if environment == XtrongEnv.global {
        return "https://xtrongv2.azurewebsites.net"
    } else if environment == XtrongEnv.local {
        return "http://192.168.1.123:8888"
    } else {
        return "http://85.105.78.106:8888"
    }
}

func getAppToken(useragent:String?) -> String {
    if let useragent = useragent {
        if useragent.contains("Android") {
            return "da8d1b95d6774599b2d83834639cdedd"
        } else {
            return "28b231165f9545c88249040d471fe21c"
        }
    } else {
        return ""
    }
}

var context:Node?

drop.post("Get") { req in
    print("Conformance app Request : \(req)")
    guard let bytes = req.body.bytes else {
        return "Get Hata"
    }
    
    let json = try JSON(bytes: bytes)
    print("Got JSON: \(json)")
    
    var params = [
        "context":[
            "applicationToken": try! Node(node:getAppToken(useragent: req.headers["User-Agent"])),
        ]
        ] as [String: Node]
    
    if let usernameParam = json["context"]?["userName"]?.string, let node = try? Node(node:usernameParam){
        params["context"]?["username"] = node
    }
    //        else {
    //            params["context"]?["username"] = try Node(node:"ervanux@gmail.com")
    //        }
    
    if let transaction = json["context"]?["transaction"]?.string, let node = try? Node(node:transaction){
        params["context"]?["transaction"] = node
    }
    
    let body = try! Node(node:params)
    
    let jsonBytes = try JSON(node: body).makeBytes()
    
    guard let op = json["op"]?.string else {
        return "Get : Operation Error"
    }
    
    var url = "\(getUrl(environment: XtrongEnv(rawValue:req.data["env"]?.string ?? "")))/xtrong/start/\(op.lowercased())"
    
    print("Get Xtrong Server Request : \(try! json.serialize(prettyPrint: true).toString())")
    let response = try drop.client.post(url,headers: ["Content-Type": "application/json"], body: Body.data(jsonBytes))
    print("Get Xtrong Server Response : \n \(String(describing: response))")
    
    context = params["context"]
    
    guard let uafRequest = try? response.json?["uafRequest"]?.makeResponse() else {
        return "There is no uafRequest on Get"
    }
    
    return response
    
    
    
}

func process(method:String,req:Request) -> ResponseRepresentable? {
    
    guard let contentType = req.headers["Content-Type"], contentType.contains("application/json"), let bytes = req.body.bytes
        else {
        return nil
    }
    
    let json = try! JSON(bytes: bytes)
    print("Got JSON: \(json)")
    
    guard let uafResponse = json["uafResponse"]?.string else {
        return nil
    }
    
    guard let data = uafResponse.data(using: String.Encoding.utf8) else {
        return nil
    }
    
    let params = try! Node(node:[
        "context":context!,
        "uafResponse" : JSON(bytes: try! data.makeBytes()).makeNode()
        ]
    )
    
    
    let url = "\(getUrl(environment: XtrongEnv(rawValue:req.data["env"]?.string ?? "")))/xtrong/process/\(method)"
    guard let paramjson = try? JSON(node:params) else {
        return nil
    }
    
    print("Process \(method) Xtrong Server Request : \(try! paramjson.serialize(prettyPrint: true).toString())")
    let response = try? drop.client.post(url,headers: ["Content-Type": "application/json"], body: paramjson)
    print("Process \(method) Xtrong Server Response : \n \(String(describing: response))")
    
    return response
    
    
}

drop.post("/Send/Reg") { req in
    if let response  = process(method:"reg",req: req) {
        return response
    }
    
    return "Send Reg Hata"
}

drop.post("/Send/Auth") { req in
    if let response  = process(method:"auth",req: req) {
        return response
    }
    
    return "Send Auth Hata"
}


drop.resource("posts", PostController())

drop.run()
